var express = require("express"),
  app = express(),
  bodyParser = require("body-parser")
  mineria = require('./mineria.js');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var router = express.Router();

app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

router.get('/blocks', function (req, res) {
  let blocks = mineria.getAllBlocks();
  res.status(200).jsonp(blocks);
});

router.post('/createTransaction', function (req, res) {
  console.log(req.body);
  let transaction = {
      fromAddress: req.body.fromAddress,
      toAddress: req.body.toAddress,
      amount: parseInt(req.body.amount)
  }
  mineria.createTransaction(transaction)
  res.status(200).jsonp(transaction);
});

router.get('/balanceOf/:address', function (req, res) {
  let balance = {
    address: req.params.address,
    balance: mineria.getBalanceOfAddress(req.params.address)
  };
  res.status(200).jsonp(balance);
});

app.use(router);

app.listen(3000, function () {
  console.log("Node server running on http://localhost:3000");
  mineria.minePendingTransactions('erik-address')
});
