
# Proyecto ECCICOIN

Este es un prototipo creado en Node js para la criptomoneda de la UECCI

## Getting Started

Para poner en marcha el proyecto se debe clonar desde este repsitorio y luego seguir lo siguiente

### Prerequisitos

Instalar Nodejs

https://nodejs.org/es/

Instalar git

https://git-scm.com

Instalar nodemon

```
npm install -g nodemon
```

### Instalando

Lo primero que debemos ejecutar el siguiente comando para instalar las dependencias

```
npm install
```

Luego ejecutamos el proyecto

```
npm start
```

En consola debe mostrar
Node server running on http://localhost:3000

# Descripción API

### API
http://localhost:3000

##### GET: Acceso a el balance de una cuenta (reemplazar 'address' por la clave del usuario)
http://localhost:3000/balanceOf/address

##### GET: Acceso a el listado de bloques    
http://localhost:3000/blocks

##### POST: Enviar transaccion (fromAddress, toAddress, amount)
http://localhost:3000/createTransaction


## bitbucket

https://bitbucket.org/feronexs/eccicoin-backend

## Autores

* Erik Araujo - erikf.araujof@ecci.edu.co
* Jaime Camargo - jaime.camargoa@ecci.edu.co
* Jullie Penagos - julliepenagosa@ecci.edu.co

## License

MIT License

